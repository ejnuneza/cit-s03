package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	
	public void init() throws ServletException{
		
		System.out.println("***************************************");
		System.out.println(" CalculatorServlet has been initialized");
		System.out.println("***************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		int num1 = Integer.parseInt(req.getParameter("n1"));
		int num2 = Integer.parseInt(req.getParameter("n2")), output = 0;
		
		
		String opr = req.getParameter("operator").toLowerCase();
		
		if(opr .toLowerCase().equals("add")) {
				output = num1+num2;
		}
		
		else if(opr .toLowerCase().equals("subtract")) {
				output = num1-num2;
		}
		
		else if(opr .toLowerCase().equals("multiply")) {
				output = num1*num2;
		}
		
		else if(opr .toLowerCase().equals("divide")) {
				output = num1/num2;
		}
		
		PrintWriter out= res.getWriter();
		out.println
		("<p>The two numbers you provided are: "+num1+","+num2+"</p>"
		+"<p>The operation that you wanted is: "+opr +"</p>"
		+"<p>The result is: "+output);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out= res.getWriter();
		out.println
		("<h1>You are now using the calculator app</h1>" 
		+ "<p>To use the app, input two numbers and an operation." + "</p>" 
		+ "<p>Hit the submit button after filling in the details."+"</p>"
		+ "<p>You will get the result shown in your browser!" + "</p>");
	}
	
	public void destroy(){
		
		System.out.println("*************************************");
		System.out.println(" CalculatorServlet has been destroyed. ");
		System.out.println("*************************************");
	}
}
